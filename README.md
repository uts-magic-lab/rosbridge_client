# How to use ROSBridge Client

[![pipeline status](https://gitlab.com/uts-unleashed/ci-example/badges/master/pipeline.svg)](https://gitlab.com/uts-unleashed/ci-example/commits/master)

## Installation via magiclab private PyPI server

```bash
magic_install ROSBridgeClient
```

## Initialization:

```python
from rosbridge_client import ROSBridgeClient
rosbridge = ROSBridgeClient(robot_ip, rosbridge_port) # default_port is 9090
```

## General functions:
```python
rosbridge.get_topics() # rostopic list
rosbridge.get_topic_type(topic_name) # rostopic type
rosbridge.get_services() # rosservice list
rosbridge.get_service_type(service_name) #rosservice type
rosbridge.get_params() # rosparam list
rosbridge.get_param_value(param_name) # rosparam get
rosbridge.set_param_value(param_name, value) # rosparam set. This will return True if setting is successful.
```

## To publish on a topic:

```python
pub = rosbridge.publisher('chatter', 'std_msgs/String')
pub.publish({'data': 'hello, world'}) # primitive data type should be wrapped into a dict with key 'data'

# unadvertise
pub.unregister()
```

## To subscribe a topic:

```python
def callback(message):
    print message.get('data')
    
sub = rosbridge.subscriber('chatter', 'std_msgs/String', callback)

# unsubscribe
sub.unregister()
```

## To call a service:

```bash
# start a service server
rosrun rospy_tutorials add_two_ints_server
```
```python
def callback(success, values):
    if success:
        print(values)

service = rosbridge.service('/add_two_ints', 'rospy_tutorials/AddTwoInts')
service.request({'a': 1, 'b': 2}, callback)
# Output: {u'sum': 3}
```

## To create a service server:
```python
def handler(args):
    """Return a tuple (result, values)."""
    return True, {'sum': args.get('a') + args.get('b')}

service_server = rosbridge.service_server('/add_two_ints', 'rospy_tutorials/AddTwoInts', handler)
# Stop service server
service_server.unregister()
```
```bash
rosservice call /add_two_ints 3 4
sum: 7
```

## To send a goal through actionlib client and listen to feedback and result:
```bash
# start a actionlib server
rosrun actionlib_tutorials fibonacci_server
```
```python
def on_result(result, status):
    print('Status: {}'.format(status.get('status')))
    print('Result: {}'.format(result.get('sequence')))

def on_feedback(feedback, status):
    print('Status: {}'.format(status.get('status')))
    print('Feedback: {}'.format(feedback.get('sequence')))
    

action_client = rosbridge.action_client('/fibonacci', 'actionlib_tutorials/FibonacciAction')
goal_id = action_client.send_goal({'order': 5}, on_result, on_feedback)

"""
Output:

Status: 1
Feedback: [0, 1, 1]
Status: 1
Feedback: [0, 1, 1, 2]
Status: 1
Feedback: [0, 1, 1, 2, 3]
Status: 1
Feedback: [0, 1, 1, 2, 3, 5]
Status: 1
Feedback: [0, 1, 1, 2, 3, 5, 8]
Status: 3
Result: [0, 1, 1, 2, 3, 5, 8]
"""

# To cancel a goal
action_client.cancel_goal(goal_id)
# Stop action client
action_client.unregister()
```