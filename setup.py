from setuptools import setup

setup(
    name='ROSBridgeClient',
    version='1.1',
    packages=['rosbridge_client'],
    package_dir={'rosbridge_client': 'rosbridge_client'},
    scripts=[],
    install_requires=['ws4py', 'PyDispatcher'],
    author='Le Kang',
    author_email='lekang8817@gmail.com',
    description='A python client for using Rosbridge',
    url='https://gitlab.com/uts-magic-lab/rosbridge_client'
)
